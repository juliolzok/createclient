"use strict";

const DYNAMODB = require("aws-sdk/clients/dynamodb");
const dynamodb = new DYNAMODB({ region: "us-east-1" });

function ageCalc(date) {
  let now = new Date();
  let birthDay = new Date(date);
  let age = now.getFullYear() - birthDay.getFullYear();
  return age;
}

function randomizer(a, b) {
  return Math.round(Math.random() * (b - a) + a);
}

exports.handler = async (event) => {
  const queue = event.Records.map((record) => record.body);

  for (const item of queue) {
    const message = JSON.parse(item);
    const body = JSON.parse(message.Message);
    console.log("body", message);

    const creditNumber = `${randomizer(0000, 9999)} - ${randomizer(
      0000,
      9999
    )} - ${randomizer(0000, 9999)} - ${randomizer(0000, 9999)}`;
    const expirationDate = `${randomizer(01, 12)}/${randomizer(21, 35)}`;
    const secCode = `${randomizer(000, 999)}`;
    let type = ageCalc(body.birthDate) > 45 ? "Gold" : "Classic";

    const dbParams = {
      ExpressionAttributeNames: {
        "#C": "creditCard",
      },
      ExpressionAttributeValues: {
        ":c": {
          M: {
            number: {
              S: creditNumber,
            },
            expiration: {
              S: expirationDate,
            },
            ccv: {
              S: secCode,
            },
            type: {
              S: type,
            },
          },
        },
      },
      Key: {
        dni: {
          S: body.dni,
        },
      },
      ReturnValues: "ALL_NEW",
      TableName: process.env.CLIENT_TABLE,
      UpdateExpression: "SET #C = :c",
    };

    try {
      const dbResult = await dynamodb.updateItem(dbParams).promise();
      console.log("dbResult", dbResult);
    } catch (error) {
      console.log(error);
      return {
        statusCode: 500,
        body: error,
      };
    }
  }

  return {
    statusCode: 200,
    body: "Tarjeta creada exitosamente",
  };
};
