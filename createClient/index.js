const DYNAMODB = require("aws-sdk/clients/dynamodb");
const SNS = require("aws-sdk/clients/sns");
const dynamodb = new DYNAMODB({ region: "us-east-1" });
const snsClient = new SNS();

function ageCalc(date) {
  let now = new Date();
  let birthDay = new Date(date);
  let age = now.getFullYear() - birthDay.getFullYear();
  return age;
}

exports.handler = async (event) => {
  if (!event.dni || !event.firstName || !event.lastName || !event.birthDate) {
    return {
      statusCode: 400,
      body: "Todos los datos son requeridos",
    };
  }

  const age = ageCalc(event.birthDate);

  if (age < 18) {
    return {
      statusCode: 400,
      body: {
        message: "El cliente debe ser mayor a 18 anios",
      },
    };
  }

  if (age > 65) {
    return {
      statusCode: 400,
      body: {
        message: "El cliente debe ser menor a 65 anios",
      },
    };
  }

  const dbParams = {
    TableName: process.env.CLIENT_TABLE,
    ReturnConsumedCapacity: "TOTAL",
    Item: {
      dni: { S: event.dni },
      firstName: { S: event.firstName },
      lastName: { S: event.lastName },
      birthDate: { S: event.birthDate },
    },
  };

  const snsParams = {
    Message: JSON.stringify(event),
    Subject: "Client created success",
    TopicArn: process.env.CLIENT_SNS,
  };

  try {
    await dynamodb.putItem(dbParams).promise();
    await snsClient.publish(snsParams).promise();
    console.log("edad", ageCalc(event.birthDate));
  } catch (error) {
    console.log(error);
    return {
      statusCode: 500,
      body: error,
    };
  }

  return {
    statusCode: 200,
    body: "Client created",
  };
};
